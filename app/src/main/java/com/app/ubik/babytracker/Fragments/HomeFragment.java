package com.app.ubik.babytracker.Fragments;

import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.ubik.babytracker.R;

import static android.content.Context.VIBRATOR_SERVICE;

public class HomeFragment extends Fragment{

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ImageView imgRecord = getView().findViewById(R.id.img_record_button);
        imgRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Vibrator myVib = (Vibrator) getContext().getSystemService(VIBRATOR_SERVICE);
                myVib.vibrate(20);
                LoadRegisterAdventures();
            }
        });
        super.onViewCreated(view, savedInstanceState);
    }

    private void LoadRegisterAdventures(){
        assert getFragmentManager() != null;
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, new AdventuresRegisterFragment(), "RegisterAdventrures")
                .remove(new HomeFragment())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

}
