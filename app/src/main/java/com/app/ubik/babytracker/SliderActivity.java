package com.app.ubik.babytracker;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import static android.view.View.TEXT_ALIGNMENT_CENTER;

public class SliderActivity extends AppCompatActivity {
    private LinearLayout mDotsLayout;
    private Button btnFinish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slider);

        ViewPager mViewPager = findViewById(R.id.ViewPager1);
        mDotsLayout = findViewById(R.id.LinearLayout1);

        SliderAdapter sliderAdapter = new SliderAdapter(this);
        mViewPager.setAdapter(sliderAdapter);
        btnFinish = findViewById(R.id.btn_finish);

        addDotsIndicator(0);
        mViewPager.addOnPageChangeListener(viewListener);
    }

    public void FinishSlide(View v)
    {
        Toast.makeText(this,"wew.. :)", Toast.LENGTH_SHORT).show();
        finish();
    }

    public void addDotsIndicator(int pos){

        TextView[] mDots = new TextView[3];

        mDotsLayout.removeAllViews();

        for(int i = 0; i < mDots.length; i++) {
            mDots[i] = new TextView(this);
            mDots[i].setText(Html.fromHtml("&#8226;"));
            mDots[i].setTextSize(45);
            mDots[i].setTextColor(getResources().getColor(R.color.colorSliderDots));
            mDotsLayout.addView(mDots[i]);
        }

        if (mDots.length > 0)
            mDots[pos].setTextColor(getResources().getColor(R.color.colorSliderDotOn));
    }

    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            addDotsIndicator(position);
            if (position == 2) {
                btnFinish.setTextColor(getResources().getColor(R.color.colorSliderDots));
            }
            else {
                btnFinish.setTextColor(getResources().getColor(R.color.colorSliderBg));
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
}
