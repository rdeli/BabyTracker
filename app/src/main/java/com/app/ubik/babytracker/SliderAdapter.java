package com.app.ubik.babytracker;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import static android.util.TypedValue.COMPLEX_UNIT_SP;

public class SliderAdapter extends PagerAdapter {

    private Context context;

    SliderAdapter(Context context){
        this.context = context;
    }

    private int[] slider_img = {
            R.drawable.first_show,
            R.drawable.graph_show,
            R.drawable.camera_show
    };

    private String[] slider_head = {
            "BABY's",
            "Best app to keep track of your baby's milestones in order to ensure a balanced development.",
            "You can take pictures and write a memories of the most important moments in your baby's life."
    };

    private String[] slider_desc = {
            "FIRST ADVENTURES",
            "",""
    };

    @Override
    public int getCount() {
        return slider_desc.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        View view = layoutInflater.inflate(R.layout.slider_layout, container, false);

        ImageView sliderImg = view.findViewById(R.id.imageView1);
        TextView sliderHead = view.findViewById(R.id.textView1);
        TextView sliderDesc = view.findViewById(R.id.textView2);

        sliderImg.setImageResource(slider_img[position]);
        if (position != 0) {
            sliderHead.setTextSize(COMPLEX_UNIT_SP,20);
        }
        else{
            sliderHead.setTextSize(COMPLEX_UNIT_SP,28);
        }
        sliderHead.setText(slider_head[position]);
        sliderDesc.setText(slider_desc[position]);

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((RelativeLayout)object);
    }
}




