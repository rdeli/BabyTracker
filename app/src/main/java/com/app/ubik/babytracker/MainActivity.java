package com.app.ubik.babytracker;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.app.ubik.babytracker.Fragments.AdventuresFragment;
import com.app.ubik.babytracker.Fragments.AnalyticsFragment;
import com.app.ubik.babytracker.Fragments.BabyFragment;
import com.app.ubik.babytracker.Fragments.HomeFragment;
import com.app.ubik.babytracker.Fragments.MilestonesFragment;
import com.app.ubik.babytracker.Fragments.SettingsFragment;
import com.app.ubik.babytracker.Fragments.TrackFragment;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx; //https://github.com/ittianyu/BottomNavigationViewEx

public class MainActivity extends AppCompatActivity {

    private Fragment fragment = null;
    private BottomNavigationViewEx bottomNavCopy = null;
    private BottomNavigationViewEx topNavCopy = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadFragment(new HomeFragment(), "HOME");

        final BottomNavigationViewEx bottomNavigationView = findViewById(R.id.bottom_navigation);
        final BottomNavigationViewEx topNavigationView = findViewById(R.id.top_navigation);
        bottomNavCopy = bottomNavigationView;
        topNavCopy = topNavigationView;

        // Welcome/FirstStart/Info page
        Slider();

        //module 'BottomNavigationViewEx' Settings
        NavBarSetStyle(topNavigationView, bottomNavigationView, false);

        //+++++++++++++++
        // BOTTOM MENU ++
        //+++++++++++++++
        NavBarFocus(bottomNavigationView, false, 0);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            private boolean FirstClick = false;
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                //int position = bottomNavigationView.getMenuItemPosition(item);
                NavBarDisableAllItemFocus(topNavigationView, topNavigationView.getItemCount());
                NavBarEnableAllItemFocus(bottomNavigationView, bottomNavigationView.getItemCount());
                if (!FirstClick) {
                    FirstClick = true;
                    NavBarFocus(bottomNavigationView, true, 0);
                }
                String TAG = "";
                switch (item.getItemId())
                {
                    case R.id.action_track:
                        fragment = new TrackFragment();
                        TAG = "track";
                        break;
                    case R.id.action_milestone:
                        fragment = new MilestonesFragment();
                        TAG = "milestones";
                        break;
                    case R.id.action_adventure:
                        fragment = new AdventuresFragment();
                        TAG = "adventure";
                        break;
                }
                return loadFragment(fragment, TAG);
            }
        });

        //++++++++++++
        // TOP MENU ++
        //++++++++++++
        NavBarFocus(topNavigationView, false, 0);
        topNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            private boolean FirstClick = false;
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                //int position = topNavigationView.getMenuItemPosition(item);
                NavBarDisableAllItemFocus(bottomNavigationView, bottomNavigationView.getItemCount());
                NavBarEnableAllItemFocus(topNavigationView, topNavigationView.getItemCount());
                if (!FirstClick) {
                    FirstClick = true;
                    NavBarFocus(topNavigationView, true, 0);
                }
                String TAG = "";
                switch (item.getItemId())
                {
                    case R.id.action_analytics:
                        fragment = new AnalyticsFragment();
                        TAG = "analytics";
                        break;
                    case R.id.action_baby:
                        fragment = new BabyFragment();
                        TAG = "baby";
                        break;
                    case R.id.action_settings:
                        fragment = new SettingsFragment();
                        TAG = "settings";
                        break;
                }
                return loadFragment(fragment,TAG);
            }
        });
    }

    @Override
    public void onBackPressed() {
        Fragment fg = getSupportFragmentManager().findFragmentByTag("HOME");
        if (fg != null && fg.isVisible()){
            super.onBackPressed();
        }
        else {
            loadFragment(new HomeFragment(), "HOME");
            NavBarDisableAllItemFocus(bottomNavCopy, bottomNavCopy.getItemCount());
            NavBarDisableAllItemFocus(topNavCopy, topNavCopy.getItemCount());
        }
    }

    public void NavBarFocus(BottomNavigationViewEx nav, boolean enable, int pos){
        if (!enable){
            nav.setIconTintList(pos, getResources().getColorStateList(R.color.colorGray1));
            nav.setTextTintList(pos, getResources().getColorStateList(R.color.colorGray1));
        }
        else{
            nav.setIconTintList(pos, getResources().getColorStateList(R.color.selector_item_primary_color));
            nav.setTextTintList(pos, getResources().getColorStateList(R.color.selector_item_primary_color));
        }

    }

    public void NavBarDisableAllItemFocus(BottomNavigationViewEx nav, int count){
        for(int i = 0; i < count; i++){
            NavBarFocus(nav,false, i);
        }
    }

    public void NavBarEnableAllItemFocus(BottomNavigationViewEx nav, int count){
        for(int i = 0; i < count; i++){
            NavBarFocus(nav,true, i);
        }
    }

    public boolean loadFragment(Fragment fragment, String tag){
        if (fragment != null){
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment, tag)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
            return true;
        }
        return false;
    }

    public void Slider(){
        Intent kappa = new Intent(this, SliderActivity.class);
        startActivity(kappa);
    }

    public void NavBarSetStyle(BottomNavigationViewEx top, BottomNavigationViewEx bottom, boolean enableAnim)
    {
        NavBarSetAnim(bottom, enableAnim);
        NavBarSetAnim(top, enableAnim);
        bottom.setLargeTextSize(15);
        bottom.setSmallTextSize(15);
        top.setIconSize(26, 26);
        top.setIconsMarginTop(40);
    }

    public void NavBarSetAnim(BottomNavigationViewEx nav, boolean enable){
        nav.enableAnimation(enable);
        nav.enableShiftingMode(enable);
        nav.enableItemShiftingMode(enable);
    }

}